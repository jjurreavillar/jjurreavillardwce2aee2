var clasificacion = null;

function creaLista(clasi)
{    
    var lista = document.getElementById("lista");
    
    lista.innerHTML = "";
    
    for (var i = 0; i < clasi.length; i++)
    {
        var linea = clasi[i].nombre + " - Puntos: " + clasi[i].puntos + " - Goles a favor: " + clasi[i].golesAFavor;
        
        var li = document.createElement("li");
        
        li.appendChild(document.createTextNode(linea));
        
        lista.appendChild(li);
    }
}

function obtenerDisponibilidad()
{
    var url = "http://hispabyte.net/DWEC/entregable2-2.php";
    var procesaError = null;
    var metodo = "POST";
    var parametros = null;
    var contentType = "application/x-www-form-urlencoded";
    var procesaCaducidad = null;
    var caducidad = null;
    var cargador = new miAjax.CargadorContenidos(
                url,
                procesaRespuesta,
                procesaError,
                procesaCarga,
                metodo,
                parametros,
                contentType,
                procesaCaducidad,
                caducidad);
    
    // Para que no actualice la página
    return false;
}

function ordenar()
{
    var orden = document.getElementById("orden").value;
    var lista = document.getElementById("lista");
    
    // Crea una copia del original
    var clasi = JSON.parse(JSON.stringify(clasificacion));
    
    switch (orden)
    {
        case "descendienteGoles":
            clasi.sort(function(a, b) { return b.golesAFavor - a.golesAFavor; }) // Descendiente
            break;
            
        case "descendientePuntos":
            clasi.sort(function(a, b) { return b.puntos - a.puntos; })
            break;
            
        case "original":
            break;
            
        default:
            alert("Índice desconocido.");
            return;
    }
    
    creaLista(clasi);
}

function procesaCarga()
{
    document.getElementById("lista").innerHTML = "Cargando...";
}

function procesaRespuesta()
{
    if (this.peticion.readyState == miAjax.READY_STATE_COMPLETE && this.peticion.status == 200)
    {
        var respuesta_json = this.peticion.responseText;
        var respuesta = JSON.parse(respuesta_json);
        
        clasificacion = respuesta;
        
        creaLista(clasificacion);
    }
}
